Source: rust-packed-simd
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-cfg-if-0.1+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Ximin Luo <infinity0@debian.org>,
 kpcyrd <git@rxv.cc>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/packed-simd]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/packed-simd
Homepage: https://github.com/rust-lang-nursery/packed_simd
X-Cargo-Crate: packed_simd

Package: librust-packed-simd-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-cfg-if-0.1+default-dev
Suggests:
 librust-packed-simd+core-arch-dev (= ${binary:Version}),
 librust-packed-simd+sleef-sys-dev (= ${binary:Version})
Provides:
 librust-packed-simd+default-dev (= ${binary:Version}),
 librust-packed-simd+into-bits-dev (= ${binary:Version}),
 librust-packed-simd+libcore-neon-dev (= ${binary:Version}),
 librust-packed-simd-0-dev (= ${binary:Version}),
 librust-packed-simd-0+default-dev (= ${binary:Version}),
 librust-packed-simd-0+into-bits-dev (= ${binary:Version}),
 librust-packed-simd-0+libcore-neon-dev (= ${binary:Version}),
 librust-packed-simd-0.3-dev (= ${binary:Version}),
 librust-packed-simd-0.3+default-dev (= ${binary:Version}),
 librust-packed-simd-0.3+into-bits-dev (= ${binary:Version}),
 librust-packed-simd-0.3+libcore-neon-dev (= ${binary:Version}),
 librust-packed-simd-0.3.3-dev (= ${binary:Version}),
 librust-packed-simd-0.3.3+default-dev (= ${binary:Version}),
 librust-packed-simd-0.3.3+into-bits-dev (= ${binary:Version}),
 librust-packed-simd-0.3.3+libcore-neon-dev (= ${binary:Version})
Description: Portable Packed SIMD vectors - Rust source code
 This package contains the source for the Rust packed_simd crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-packed-simd+core-arch-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-packed-simd-dev (= ${binary:Version}),
 librust-core-arch-0.1+default-dev (>= 0.1.3-~~)
Provides:
 librust-packed-simd-0+core-arch-dev (= ${binary:Version}),
 librust-packed-simd-0.3+core-arch-dev (= ${binary:Version}),
 librust-packed-simd-0.3.3+core-arch-dev (= ${binary:Version})
Description: Portable Packed SIMD vectors - feature "core_arch"
 This metapackage enables feature "core_arch" for the Rust packed_simd crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-packed-simd+sleef-sys-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-packed-simd-dev (= ${binary:Version}),
 librust-sleef-sys-0.1+default-dev (>= 0.1.2-~~)
Provides:
 librust-packed-simd-0+sleef-sys-dev (= ${binary:Version}),
 librust-packed-simd-0.3+sleef-sys-dev (= ${binary:Version}),
 librust-packed-simd-0.3.3+sleef-sys-dev (= ${binary:Version})
Description: Portable Packed SIMD vectors - feature "sleef-sys"
 This metapackage enables feature "sleef-sys" for the Rust packed_simd crate, by
 pulling in any additional dependencies needed by that feature.
